import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Banco } from './datos/Banco';

@Injectable({
  providedIn: 'root'
})
export class BancoService {
	//https://jsonplaceholder.typicode.com/posts/1
	//http://localhost:8088/Slim/usuarios
	 //url = 'http://localhost:8099/sbanco';
   //url2 = 'http://localhost:8099/api';
  //urlcuenta = 'http://localhost:3000/api/cuentasXtarjeta';


 // url = 'http://localhost:8099/sbanco';
   //url2 = 'http://localhost:8099/api';

   url = 'http://localhost:3000/api/sbanco';
   url2 = 'http://localhost:3000/api/create';
  
  constructor(private http: HttpClient ) { }
  //constructor() { }

 obtenerDatos(){
    return this.http.get<Banco[]>(this.url);
  }
  enviarDatos(user: Banco){
  	return this.http.post<any>(this.url2, user);
  }
}