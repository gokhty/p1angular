import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.sass']

})
export class NavComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

  ver = false;

  abrirCarrito() {
   if(this.ver == true){
			 this.ver=false;
			 document.querySelector("#idvercarrito").classList.add('clsvercarrito');
		 }else{
			 this.ver=true;
			 document.querySelector("#idvercarrito").classList.remove('clsvercarrito');
		 }
  }

}
