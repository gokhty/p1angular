import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CtaComponent } from './cta/cta.component';

//------------

import { Routes, RouterModule } from '@angular/router';
import { NavComponent } from './nav/nav.component';
import { MainComponent } from './main/main.component';

// 1http
import { HttpClientModule } from '@angular/common/http';
//2http
import { BancoService } from './banco.service';

//formulario html
import { FormsModule } from '@angular/forms';
import { CarritoComponent } from './carrito/carrito.component';
import { GaleriaComponent } from './galeria/galeria.component';
import { ComprarComponent } from './comprar/comprar.component';

import { HashLocationStrategy, LocationStrategy } from '@angular/common';

const routes: Routes = [
  { path: 'comprar', component: ComprarComponent },
  { path: 'galeria', component: GaleriaComponent },
  { path: 'cta', component: CtaComponent },
  { path: 'psm', component: MainComponent }
 ];


@NgModule({
  declarations: [
    AppComponent,
    CtaComponent,
    NavComponent,
    MainComponent,
    CarritoComponent,
    GaleriaComponent,
    ComprarComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    RouterModule.forRoot(routes)
  ],
  providers: [BancoService, {provide: LocationStrategy, useClass: HashLocationStrategy}],
  bootstrap: [AppComponent]
})
export class AppModule { }
