import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-comprar',
  templateUrl: './comprar.component.html',
  styleUrls: ['./comprar.component.sass']
})
export class ComprarComponent implements OnInit {
	tab: number = 0;
  constructor() { }

  ngOnInit() {
  }
  selection(num: number) {
  	this.tab = num;
  }

}
